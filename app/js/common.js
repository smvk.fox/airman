import $ from 'jquery'; window.jQuery = $; window.$ = $
// require('./other_script.js') // Require Other Script(s) from app/js folder Example

document.addEventListener('DOMContentLoaded', () => {

//Custom JS
//Responsive menu
$('.icon-menu').on('click', function() {
    $('.nav').slideToggle(300, function(){
         if( $(this).css('display') === "none"){
             $(this).removeAttr('style');
         }
    });
});
//Responsive menu

//Form
var form = document.getElementById("my-form");
    
    async function handleSubmit(event) {
      event.preventDefault();
      var status = document.getElementById("my-form-status");
      var data = new FormData(event.target);
      fetch(event.target.action, {
        method: form.method,
        body: data,
        headers: {
            'Accept': 'application/json'
        }
      }).then(response => {
        status.innerHTML = "Спасибо за заказ!";
        form.reset()
      }).catch(error => {
        status.innerHTML = "Ошибка!"
      });
    }
    form.addEventListener("submit", handleSubmit)
//Form
  })
